# John Edge



## About Me

Just some dude. A [standard nerd](https://youtu.be/JwU7SF0i0Ws?feature=shared).

Also: Senior Site Reliability Engineer on the [US Public Sector Services](https://handbook.gitlab.com/handbook/engineering/infrastructure/team/gitlab-dedicated/#team-members) team at GitLab.


## Getting in touch

You can contact me a few ways:

- GitLab Issues (preferred) by pinging @jedge1
- [Slack](https://gitlab.slack.com/team/U04QM64838T)
- [LinkedIn](https://www.linkedin.com/in/john-edge-75761215/) (if you're just wanting to connect professionally)



K thx bye :wave:
